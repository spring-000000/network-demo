package com.qf.bio;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Gu
 * @create 2021-05-27-22:24
 * bio的服务器
 */
public class BioServer {

    public static List<Socket> socketList = new ArrayList<Socket>();

    public static void main(String[] args) throws IOException {
        //首先创建socket服务对象
        ServerSocket serverSocket = new ServerSocket(8081);
        //服务端要接收客户端的连接-阻塞式的
        //一旦有客户端连接,该方法会返回该客户端的socket对象,如果没有客户端连接就会一直阻塞
        while (true) {
            final Socket socket = serverSocket.accept();
            //保存当前的连接对象
            socketList.add(socket);
            System.out.println("有一个客户端连接~~~~");

            new Thread() {
                @Override
                public void run() {
                    while (true) {
                        try {
                            //获得客户端请求
                            InputStream inputStream = socket.getInputStream();
                            byte[] bytes = new byte[10 * 1024];
                            int len = 0;
                            len = inputStream.read(bytes);
                            String s = new String(bytes, 0, len);
                            System.out.println("获得客户端的请求数据: " + s + "并且将数据群发给其他客户端");

//                            群发给其他的客户端
                            for (Socket sock : socketList) {
                                if (sock != socket) {
                                    //不发送给自己
                                    sock.getOutputStream().write(s.getBytes());
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        }
    }
}
