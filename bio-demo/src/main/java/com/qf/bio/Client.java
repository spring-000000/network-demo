package com.qf.bio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * @author Gu
 * @create 2021-05-27-22:44
 */
public class Client {
    public static void main(String[] args) throws IOException {
//        创建客户端socket对象,并且连接服务器
        final Socket socket = new Socket("127.0.0.1", 8081);
        Scanner scanner = new Scanner(System.in);

        //            开启一个子线程监听服务端发送的消息
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        //        连接服务端
                        InputStream inputStream = null;
                        inputStream = socket.getInputStream();
                        byte[] bytes = new byte[10 * 1024];
                        int len = inputStream.read(bytes);
                        System.out.println("接收到服务器的响应" + new String(bytes, 0, len));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();

        while (true) {
//        发消息
            OutputStream outputStream = socket.getOutputStream();
//            outputStream.write("hello~~~".getBytes());
            String next = scanner.next();
            outputStream.write(next.getBytes());
            outputStream.flush();

//            socket.close();
        }
    }
}
