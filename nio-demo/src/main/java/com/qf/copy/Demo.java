package com.qf.copy;

import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @author Gu
 * @create 2021-05-28-1:59
 */
public class Demo {
    public static void main(String[] args) {
        try (
                FileChannel fileChannel = new FileOutputStream("").getChannel();
                FileChannel output = new FileOutputStream("").getChannel()
        ) {
            //        准备buffer 位置为0 容量最大 接线指到容量
            ByteBuffer byteBuffer = ByteBuffer.allocate(10 * 1024);
//        循环写入数据到bytebuffer
            while (fileChannel.read(byteBuffer) != -1) {
//            位置指向写入的数据的末端 容量最大 界限指到容量
                //界限移动到位置为0地方,位置重置为0 为读取数据做准备
                byteBuffer.flip();
//            读取bytebuffer的数据 写入输出管道
                output.write(byteBuffer);
//            重置bytebuffer
                byteBuffer.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
