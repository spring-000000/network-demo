package com.qf.msg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Scanner;

/**
 * @author Gu
 * @create 2021-05-28-2:44
 */
public class NioClient {
    public static void main(String[] args) throws IOException {
        SocketChannel socketChannel = SocketChannel.open();
        socketChannel.connect(new InetSocketAddress("127.0.0.1", 8081));
//        设置为阻塞式
        socketChannel.configureBlocking(true);

        new Thread(() -> {
            try {
                while (true) {
                    ByteBuffer byteBuffer = ByteBuffer.allocate(10 * 1024);
                    socketChannel.read(byteBuffer);
//                    打印数据
                    byteBuffer.flip();
                    byte[] array = byteBuffer.array();
                    System.out.println("读取到服务器消息: " + new String(array));
                }
            } catch (IOException e) {
                System.out.println("服务读取异常!!!! ");
                e.printStackTrace();
            }
        }).start();

//        主线程本身是 死循环写入
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("输入你要发送的消息");
            String next = scanner.next();

            ByteBuffer byteBuffer = ByteBuffer.allocate(1024 * 10);
            byteBuffer.put(next.getBytes(),0,next.getBytes().length);
            byteBuffer.flip();
            socketChannel.write(byteBuffer);
        }
    }
}
