package com.qf.msg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Gu
 * @create 2021-05-28-2:16
 */
public class NioServer {
    public static void main(String[] args) throws IOException {

        List<SocketChannel> socketChannels = new ArrayList<>();

//        创建一个服务端的channel
        ServerSocketChannel open = ServerSocketChannel.open();
//        绑定端口
        open.bind(new InetSocketAddress(8081));
//        设置channel的类型 设置当前为非阻塞模式
        open.configureBlocking(false);

//        创建一个多路复用器
        Selector selector = Selector.open();
//        将channel注册到多路复用器 注册到多路复用器 注册的动作为 等待客户端连接
        open.register(selector, SelectionKey.OP_ACCEPT);

//        主线程轮询多路复用器
        while (true) {
            int select = selector.select();
            if (select > 0) {
//                有客户端发生某个行为  从多路复用器中返回有动作的channel集合
                Set<SelectionKey> selectionKeys = selector.selectedKeys();
//                循环处理channel 移除旧集合 避免移除集合时报错
                for (SelectionKey selectionKey : new HashSet<>(selectionKeys)) {
//                    从集合中移除当前的channel
                    selectionKeys.remove(selectionKey);

//                    判断当前发生了什么动作
                    if (selectionKey.isAcceptable()) {
//                        说明有一个新客户端连接了
                        ServerSocketChannel serverchannel = (ServerSocketChannel) selectionKey.channel();
//                        等待客户端连接
                        SocketChannel socketChannel = serverchannel.accept();
                        System.out.println("接收到客户端的连接.....");
//                        将客户端的channel注册到多路复用器上 非阻塞模式
                        socketChannel.configureBlocking(false);
//                        注册多路复用器
                        socketChannel.register(selector, SelectionKey.OP_ACCEPT);

//                        保存管理客户端channel集合
                        socketChannels.add(socketChannel);
                    } else if (selectionKey.isReadable()) {
//                        说明有一个客户端发送了消息
                        SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
//                        读取channel中的数据
                        ByteBuffer byteBuffer = ByteBuffer.allocate(10 * 2048);
                        socketChannel.read(byteBuffer);
//                        重置初始位置
                        byteBuffer.flip();
                        byte[] bytes = byteBuffer.array();
                        System.out.println("接收到的客户端数据: " + new String(bytes));

                        for (SocketChannel otherChannel : socketChannels) {
                            if (otherChannel != socketChannel) {
                                //每次写时候重置位置
                                byteBuffer.flip();
                                otherChannel.write(byteBuffer);
                            }
                        }
                    }
                }
            }
        }
    }
}
