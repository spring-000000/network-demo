package com.powersu.httpfileserver;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.stream.ChunkedFile;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @author Gu
 * @create 2021-05-31-0:27
 */
public class MyFileChannelHandler extends SimpleChannelInboundHandler<FullHttpRequest> {

    /**
     * 开放本地路径
     */
    private String path = "D:\\";

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, FullHttpRequest request) throws Exception {
//        获得请求类型
        HttpMethod method = request.method();

//        只接收get请求
        if (!method.name().equalsIgnoreCase("GET")) {
//        非get请求 拒绝
            setErro(ctx, "请求类型异常! 别乱搞了!");
        }

//        获得请求的url
        String uri = request.uri();

//        必须得进行url解码
        uri = URLDecoder.decode(uri, "UTF-8");

        System.out.println("请求的url: " + uri);
//        判断请求的url是否存在
        File file = new File(path, uri);
        if (!file.exists()) {
            setErro(ctx, "请求的路径不存在! 请不要乱来!");
            return;
        }

        if (file.isFile()) {
//            请求是文件 开始下载
            fileHandler(ctx, file);
        } else {
//            请求是文件夹 返回文件夹下文件列表
            dirHandler(ctx, file, uri);
        }
    }

    /**
     * 文件的处理方式
     *
     * @param ctx
     * @param file
     */
    private void fileHandler(ChannelHandlerContext ctx, File file) {
//      下载该文件 读取该文件 netty提供了分块文件

//        编写相应头和响应行
        DefaultFullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
//        相应体的内容
        response.headers().add("Content-Type", "application/octet-stream");
//        设置响应体的大小
        response.headers().add("Content-Length", file.length());
//        返回响应头和响应码给客户端
        ctx.writeAndFlush(response);

//        下载的文件(响应体) 分块传给客户端
        try {
            ChunkedFile chunkFile = new ChunkedFile(file, 1024 * 1024);
            ChannelFuture channelFuture = ctx.writeAndFlush(chunkFile);
//            回调方法后关闭连接
            channelFuture.addListener(new ChannelFutureListener() {
                @Override
                public void operationComplete(ChannelFuture channelFuture) throws Exception {
                    if (channelFuture.isSuccess()) {
                        System.out.println("文件下载完成");
                        ctx.close();
                    }
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 路径的处理方式
     *
     * @param ctx
     * @param file
     */
    private void dirHandler(ChannelHandlerContext ctx, File file, String uri) {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("<ul>");
//      获取文件下的子文件
        File[] files = file.listFiles();
//        遍历当前文件
        Arrays.stream(files).forEach(f -> {
            stringBuilder.append("<li><a href='").append(uri.equals("/") ? "" : uri).append("/").append(f.getName()).append("'>")
                    .append("(").append(f.isFile() ? "文件" : "文件夹").append(")")
                    .append(f.getName()).append("</a></li>");
        });

        stringBuilder.append("</ul>");

//        响应给客户端
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);
        response.headers().add("Content-Type", "text/html;charset=utf-8");
        response.content().writeBytes(stringBuilder.toString().getBytes(StandardCharsets.UTF_8));

//        返回响应
        ctx.writeAndFlush(response);
        ctx.close();
    }

    /**
     * 报错处理
     *
     * @param ctx
     * @param errorMsg
     */
    private void setErro(ChannelHandlerContext ctx, String errorMsg) {
        //        响应给客户端
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.BAD_REQUEST);
        response.headers().add("Content-Type", "text/html;charset=utf-8");
        response.content().writeBytes(errorMsg.getBytes(StandardCharsets.UTF_8));

//        返回响应
        ctx.writeAndFlush(response);
        ctx.close();

    }
}
