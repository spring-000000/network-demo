package com.powersu.httpfileserver;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.stream.ChunkedWriteHandler;

/**
 * @author Gu
 * @create 2021-05-31-0:26
 */
public class HttpFileServer {
    public static void main(String[] args) {
        EventLoopGroup master = new NioEventLoopGroup();
        EventLoopGroup slave = new NioEventLoopGroup();

        ServerBootstrap serverBootstrap = new ServerBootstrap()
                .group(master, slave)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer() {

                    @Override
                    protected void initChannel(Channel channel) throws Exception {
//                        自定义处理器
                        ChannelPipeline pipeline = channel.pipeline();

//                        添加分块文件处理器编码器
                        pipeline.addLast(new ChunkedWriteHandler());

                        pipeline.addLast(new HttpServerCodec());
//                        这是一个http请求的聚合器 该聚合器会将所有的httorequest httpcontent lasthttpcontent聚合成为一个fullhttprequest对象
                        pipeline.addLast(new HttpObjectAggregator(10 * 1024 * 1024));
                        pipeline.addLast(new MyFileChannelHandler());
                    }
                });
        try {
            serverBootstrap.bind(8888).sync();
            System.out.println("端口绑定成功,服务已经启动!");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
